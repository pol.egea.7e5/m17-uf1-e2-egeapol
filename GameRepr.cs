﻿using System;
using System.Collections.Generic;
using System.Text;
using GameTools;

namespace MyFirstProgram
{
    class GameRepr:GameEngine
    {
        RecolorMatrix mr=new RecolorMatrix(50, 100);
        protected override void Start()
        {
            mr.BlankMatrix();
            mr.CleanTheMatrix();
            ListenKeyboard();
        }
        protected override void Update()
        {
            Random rnd = new Random();
            for(int i = mr.TheMatrix.GetLength(0)-1; i >= 1; i--)
            {
                for(int j = 0; j <= mr.TheMatrix.GetLength(1)-1; j++)
                {
                    mr.TheMatrix[i, j] = mr.TheMatrix[i - 1, j];
                }
                
            }
            for (int j = 0; j <= mr.TheMatrix.GetLength(1) - 1; j++)
            {
                mr.TheMatrix[0, j] = (mr.TheMatrix[1, j] != '0') ? (rnd.Next(0, 3) != 0) ? (char)rnd.Next(48, 57) : '0' : (rnd.Next(0, 8) == 0) ? (char)rnd.Next(48, 58) : '0';
            }
            mr.PrintMatrix();
        }
        protected override void Exit()
        {
            mr.BlankMatrix();
            mr.CleanTheMatrix();
        }
    }
}
