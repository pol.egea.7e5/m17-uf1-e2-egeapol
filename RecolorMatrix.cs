﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyFirstProgram
{
    class RecolorMatrix:MatrixRepresentation
    {
        public RecolorMatrix()
        {
            InitMatrix(null, null);
        }
        public RecolorMatrix(int width, int height)
        {
            InitMatrix(width, height);
        }
        public override void PrintMatrix()
        {
            Console.Clear();
            for (int x = 0; x < _theMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < _theMatrix.GetLength(1); y++)
                {
                    if (x == _theMatrix.GetLength(0) - 1 && _theMatrix[x, y] != '0' || (x != _theMatrix.GetLength(0) - 1 && _theMatrix[x, y] != '0' && _theMatrix[x + 1, y] == '0'))
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($" {_theMatrix[x, y]}");
                    }
                    else if (x!=_theMatrix.GetLength(0)-1&&(_theMatrix[x + 1, y] != '0' && _theMatrix[x, y] != '0'))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" {_theMatrix[x, y]}");
                    }
                    else
                    {
                        Console.Write($"  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
